const mongoose = require("mongoose");
const connectDB = require("./config-DB/dbmooge");
const Product = require("./filedata/Product");
const product = require("./models/product");
const user = require ('./filedata/user.js');
const User = require('./models/User');

connectDB();

const importData = async () =>{
    try{
        await User.deleteMany();
        await product.deleteMany();
        const createUsers = await User.insertMany(user);

        const adminUser = createUsers[0]._id;

        await product.insertMany(Product);
        console.log("Date Imported");
        process.exit();
    }catch(error){
        console.log(error);
        process.exit(1);
    }
};

const deleteData = async () => {
    try{
      await User.deleteMany();
      await Product.deleteMany();
      console.log("Data Deleted");
      process.exit();
    }catch(error){
        console.log(error);
        process.exit(1);
    }
};

if(process.argv[2] === "-d") {
    deleteData();
}else{
    importData();
}