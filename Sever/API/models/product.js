const mongoose = require("mongoose");

const ProductSchema = mongoose.Schema({
    product_img:{
        type:String,
        required:true,
    },
    model:{
        type:String,
        required:true,
    },
    name:{
        type:String,
        required:true,
    },
    price:{
        type:Number,
        required:true,
        default:0,
    },
    description:{
        type:String,
        required:true,
    },
},
{timetamps:true});

const Product = mongoose.model("Product", ProductSchema);
module.exports = Product;