const bcrypt = require("bcryptjs");

const users = [
  {
    name: "Admin User",
    email: "admin@example.com",
    password: bcrypt.hashSync("123456", 10),
    Admin: true,
  },
  {
    name: "Sam Sovan",
    email: "sovan@example.com",
    password: bcrypt.hashSync("123456", 10),
  },
  {
    name: "LIM Pong",
    email: "pong@example.com",
    password: bcrypt.hashSync("123456", 10),
  },
];

module.exports = users;