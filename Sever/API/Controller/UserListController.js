const asyncHandler = require("express-async-handler");
const tokengenerate = require("../Token/tokengenerate");
const User = require("../models/User");

const GettheUser = asyncHandler(async (req, res) =>{
    try{
        const response = await User.find({})
        res.json(response);
    }catch(error){
        throw new Error(`Authorization Not right: ${error}`);
    }
});

const UserRegister = asyncHandler(async(req, res)=>{
    const {name, password, email} = req.body;

    const Existuser = await User.findOne({email});
    if(Existuser){
        res.status(400);
        throw new Error("EMAIL ALREADY EXISTED");
    }
    const user = await User.create({
        name,
        email,
        password,
    });
    if(user){
        res.status(201).json({
            _id:user._id,
            name:user.name,
            email:user.email,
            Admin: user.Admin,
            token:tokengenerate(user.email),
        });
    }else{
        res.status(400);
        throw new Error("Data Invalid");
    }
});

const UserLogin = asyncHandler(async (req, res)=>{
    const {email,password} = req.body;
    const isUser = await User.findOne({email});

    if(isUser &&(await isUser.matchPassword(password))){
        res.json({
            name:isUser.name,
            email:isUser.email,
            Admin:isUser.Admin,
            token: tokengenerate(isUser.email),
        });
    }else{
        res.status(401);
        throw new Error("Wrong Email or Password");
    }
});
const getUserProfile = asyncHandler(async (req, res) => {
    const user = await User.findById(req.user._id);
  
    if (user) {
      res.json({
        _id: user._id,
        name: user.name,
        email: user.email,
        isAdmin: user.isAdmin,
      });
    } else {
      res.status(401);
      throw new Error("User not found");
    }
  });
const DeleteUser = asyncHandler(async (req, res) => {
    try {
        await User.deleteOne({_id:req.params.id})
        res.json({message:"Delete Success"})
    } catch (error) {
        res.status(401);
        throw new Error("User not found");
    }
})
module.exports = {
    getUserProfile,
    UserLogin,
    UserRegister,
    GettheUser,
    DeleteUser,
};