const express = require('express');
const connectDB = require("./config-DB/dbmooge")
const bodyPaser =  require('body-parser');
const cors = require("cors");
const dotenv = require("dotenv");
const Users  = require("./Router/Users");
const User = require('./models/User');
const productRouter = require('./Router/productRoute');


dotenv.config();

connectDB();
const app = express();
app.use(bodyPaser.json());
app.use(bodyPaser.urlencoded({ extended:false,}));


app.use("/public", express.static("public"));


app.use(cors());
app.use(express.json());

app.get('/api' , (req , res)=>{
   res.send('API is sending....');
});

app.use("/api/user", Users);
app.use("/api/products", productRouter);
const port = process.env.PORT || 3000;
app.listen(port, () => {
    console.log(
      `Server is running in ${process.env.NODE_ENV} mode on port ${port}`
    );
  });