const express = require('express')
const router = express.Router();

const {
    UserLogin,
    UserRegister,
    GettheUser,
    getUserProfile,
    DeleteUser
  } = require("../Controller/UserListController");
  
  const protect = require("../Middleware/authware");

  router.route("/").post(UserRegister);
  router.post("/login", UserLogin);
  
  router.get("/", GettheUser);
  router.route("/profile").get(protect, getUserProfile);

  router.delete("/:id",DeleteUser);
module.exports = router;    