const express = require('express');
const multer = require('multer');
const Product = require('../models/product');
const router = express.Router();


router.get(
   "/",
  async (req, res) => {
        const products = await Product.find({});
       res.json(products);
   }
);

// router.get(
//     "/:id",
//   async (req, res) => {
//         const products = await Product.findById(req.param.id);
//         if(products){
//             res.json(products);
//         }else{
//             res.status(404);
//             throw new Error("Can't find Product");
//         }
//     }
// );

router.patch(
    "/:id",
    async (req, res) => {
        try{
            await Product.updateOne({ _id: req.params.id }, { $set: req.body });
            res.json({message:"Updata Success"});
        }catch(error){
            throw new Error(`Product not Found ${error}`);
        }
    }
);
router.delete(
    "/:id",
    async (req, res) => {
        try {
            await Product.findByIdAndDelete(req.params.id);
            res.json({ message: "Deleted Successfully" });
          } catch (error) {
            throw new Error(`Product not found ${error}`);
          }
        }
);

const DIR = './public/';

const storage = multer.diskStorage({
    destination:(req, file, cd)=>{
        cd(null,DIR);
    },
    filename: (req, file, cd)=>{
        const filename = file.originalname.toLowerCase().split(" ").join("-");
        cd(null,filename);
    },
});

var upload = multer({
    storage:storage,
});



router.post("/upload", upload.single("file"), async (req, res, next) => {
       const url = req.protocol + "://" + req.get("host");
        const reqFiles = url + "/public/" + req.file.filename;
   
    const newProduct = new Product({
      model:req.body.model,
      name: req.body.name,
      product_img: reqFiles,
      description: req.body.description,
      price:req.body.price,
    });
    await newProduct.save();
  });
module.exports = router;