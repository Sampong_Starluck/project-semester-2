import axios from 'axios';

export default{
    state(){
        return{
            products:[],
            product:null,
        };
    },
    mutations:{
        setProducts(state, payload) {
            state.products = payload;
        },
        setProduct(state, payload) {
            state.product = payload;
        },
        removeProduct(state, payload) {
            state.products = state.products.filter(products => products.id !== payload);
        },

    },
    actions:{
        async fetchProducts(context) {
            const res = await axios.get('http://localhost:3000/api/products');
            context.commit('setProducts', res.data);
            console.log(res.data);
        },
        async fetchProduct(context,id) {
            const res = await axios.get(`http://localhost:3000/api/products/${id}`);
            console.log(res.data);
            context.commit('setProduct',res.data);
        },
        async updateProduct(context, sub){
            await axios.patch(`http://localhost:3000/api/products/${sub._id}`,{
                name:sub.name,
                model:sub.model,
                price:sub.price,
                description:sub.description
            });
        },
        async DeleteProduct(context, id) {
            await axios.delete(`http://localhost:3000/api/products/${id}`);
            context.commit('DeleteProduct', id);
        },
    },
    getters:{
       getProducts(state){
           return state.products;
       },
       getProduct(state){
           return state.product;
       },
    }
}