import axios from 'axios';

export default {
  state() {
    return {
      token: null,
      userData: null,
      errmessage: null
    };
  },
  getters: {
    getUser(state) {
      return state.userData;
    },
    getErr(state) {
      return state.errmessage;
    },
    isAuth(state) {
      return state.token === null ? false : true;
    }
  },
  mutations: {
    set_token(state, payload) {
      state.token = payload;
    },
    set_user(state, payload) {
      state.userData = payload;
    },
    set_err(state, payload) {
      state.errmessage = payload;
    }
  },
  actions: {
    async Signin(context, formData) {
      try {
        const res = await axios.post(
          'http://localhost:3000/api/user/login',
          formData
        );
        console.log(res.data.token);
        context.dispatch('Attempt', res.data.token);
        context.commit('set_user',res.data)
        context.commit('set_err', null);
      } catch (error) {
        context.commit('set_err', error.response.data.message);
      }
    },
    async Attempt({ commit, state }, token) {
      if (token) {
        commit('set_token', token);
      }

      if (!state.token) {
        return;
      }
      
    },
    SignOut({ commit }) {
      commit('set_token', null);
      commit('set_user', null);
    },
    async signUp(context, data) {
      try {
        const res = await axios.post('http://localhost:3000/api/user', data);
        context.dispatch('Attempt', res.data.token);
        context.commit('set_user', res.data);
        context.commit('set_err', null);
      } catch (error) {
        context.commit('set_err', error.response.data.message);
      }
    }
  }
};