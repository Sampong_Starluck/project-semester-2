import axios from 'axios';
export default {
  state() {
    return {
      users: []
    };
  },
  mutations: {
    setUsers(state, payload) {
      state.users = payload;
    },
    removeUser(state, payload) {
     state.users = state.users.filter(users => users.name !== payload);
  }
  },
  actions: {
    async fetchUsers(context) {
      const res = await axios.get('http://localhost:3000/api/user');
      context.commit('setUsers', res.data);
    },
   async DelteUser(context, id) {
      await axios.delete(`http://localhost:3000/api/user/${id}`);
      context.commit('DeleteUsers', id);
    }
  },
  getters: {
    getUsers(state) {
      return state.users;
    }
  }
};