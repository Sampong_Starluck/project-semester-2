import { createStore } from "vuex";
import Authentication from "./Authentication.js"
import User from "./user.js"
import Product from "./Product/index.js";


const store = createStore({
  modules:{
    Authentication:Authentication,
    User:User,
    products:Product
  }
});

export default store;