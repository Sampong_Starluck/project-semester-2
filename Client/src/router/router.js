import {createWebHistory, createRouter} from 'vue-router';
import HelloWorld from "../components/HelloWorld.vue";
import carlist from "../components/carlist.vue";
import About from "../components/About.vue";
import blog from "../components/blog.vue";
import Login from "../components/Login.vue";
import Register from "../components/Register.vue";
import Contact from '@/components/Contact';
import admin from '@/components/admin';
import car from '../components/car.vue';
import user from '../components/user'
import product from '../components/product'
import MainAdmin from '../components/MainAdmin.vue'
import uploadProduct from '../components/uploadProduct.vue'
import blogNews from '../components/blogNews'
import message from '../components/message.vue'


import store from '@/store'

// const routes = [
//     {
//         path: "/",
//         name: "HelloWorld",
//         component: HelloWorld
//     },
//     {
//         path: "/carlist",
//         name: "carlist",
//         component: carlist
//     },
//     {
//         path: "/About",
//         name: "About",
//         component: About
//     },
//     {
//         path: "/blog",
//         name: "blog",
//         component: blog
//     },
//     {
//         path: "/Login",
//         name: "Login",
//         component: Login
//     },
//     {
//         path: "/Register",
//         name: "Register",
//         component: Register
//     },
//     {
//         path: "/Contact",
//         name: "Contact",
//         component: Contact
//     },
//     {
//         path: "/car",
//         name: "car",
//         component: car
//     },
//     {
//         path: "/blogNews",
//         name: "blogNews",
//         component: blogNews
//     },
//     {
//         path: "/admin",
//         name: "admin",
//         component: MainAdmin,
//         redirect: "/admin/dashboard",
//         children:[
//             {
//             path: "dashboard",
//             name: "dashboard",
//             component: admin,
//             },
//             {path: "user",
//             name: "user",
//             component: user},
//             {
//                 path: "product",
//                 name: "product",
//                 component: product
//             },
//             {
//                 path: "uploadProduct",
//                 name: "uploadProduct",
//                 component: uploadProduct
//             },
//         ]
//     },
   
// ];
const router = createRouter ({
    history: createWebHistory(process.env.BASE_URL), 
    routes:[
        {
            path: "/",
            name: "HelloWorld",
            component: HelloWorld
        },
        {
            path: "/carlist",
            name: "carlist",
            component: carlist
        },
        {
            path: "/About",
            name: "About",
            component: About
        },
        {
            path: "/blog",
            name: "blog",
            component: blog
        },
        {
            path: "/Login",
            name: "Login",
            component: Login
        },
        {
            path: "/Register",
            name: "Register",
            component: Register
        },
        {
            path: "/Contact",
            name: "Contact",
            component: Contact
        },
        {
            path: "/car",
            name: "car",
            component: car
        },
        {
            path: "/blogNews",
            name: "blogNews",
            component: blogNews
            
        },
        {
            path: "/message",
            name: "message",
            component: message
            
        },
        {
            path: "/admin",
            name: "admin",
            component: MainAdmin,
            beforeEnter:(to, from, next)=>{
                if(!store.getters['isAuth']){
                return next({name:"Login"});
                }
                next();
            },
            redirect: "/admin/dashboard",
            children:[
                {
                path: "dashboard",
                name: "dashboard",
                component: admin,
                },
                {path: "user",
                name: "user",
                component: user},
                {
                    path: "product",
                    name: "product",
                    component: product
                },
                {
                    path: "uploadProduct",
                    name: "uploadProduct",
                    component: uploadProduct
                },
            ]
        }
    ]});

export default router;